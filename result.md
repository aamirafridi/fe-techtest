# Result

### [DEMO](https://aamirafridi.gitlab.io/fe-techtest/)

## Notes

- Covered all the requirements of the test
- Total time spent on achieving the result is under 2 hours (in intervals) although it includes about 40mins in setting up React and typescript plus adding linting packages. I could use `create-react-app` but just wanted to learn how you can add react and typescript to a basic node project 😛
- I picked react (with typescript) because react is efficient in avoiding useless re-renders for a large amount of DOM elements, for example in this APP, 144 buttons. With native javascript, you will have to be careful about the re-renders when the user clicks a button to make sure you don't re-render all buttons with different or same attributes.
- I prefer keeping tests and component in a parent folder e.g. in this case App and have `App/**tests**` for tests but for simplicity I just used the test folder outside the src so my preferred folder structure should look like this

```
src
|- index.tsx
|__ App
|-App.tsx
|_ _tests_
|- App.test.tsx
```

- I could have improved the styles with some nice fonts and animations to make the app more fun for the kids
- Linting and prettier added and new scripts added in package.json to use them (attached with git hooks too)
- If the browser doesn't support CSS grid, it will gracefully fallback to fluid layout and still rendered nicely on any device (graceful degradation)
- I was so tempted to try CSS only solution with checkboxes after reading [this](https://alistapart.com/article/using-css-mod-queries-with-range-selectors/) (just for fun 🙈)
- **I tried my best to achieve the result with the least amount of react and CSS code**
- Fully accessible and works with keyboard. Instead of using `aria-pressed` I could use `role="switch" aria-checked="true"`
  ![image](https://user-images.githubusercontent.com/55896/99861832-88c71c00-2b8f-11eb-8473-501e68928143.png)

- I could have created a Button component but that would be `premature optimization`. There is no performance issue the way the `App.tsx` file is.
  Check below the lighthouse reports:
  ![image](https://user-images.githubusercontent.com/55896/99862158-9cbf4d80-2b90-11eb-9a01-791062d868d8.png)

![image](https://user-images.githubusercontent.com/55896/99862239-d7c18100-2b90-11eb-9239-27c0dd94cb7e.png)

![image](https://user-images.githubusercontent.com/55896/99862359-50c0d880-2b91-11eb-934e-1e6eaa1100e1.png)

## Git hooks

### Pre-commit

![image](https://user-images.githubusercontent.com/55896/99861436-ed817700-2b8d-11eb-85bb-4fb7db685b7b.png)

### Pre-push

![image](https://user-images.githubusercontent.com/55896/99861482-17d33480-2b8e-11eb-885c-8073d58a30e2.png)

## Available scripts under `npm run`

- start
- test
- test:watch
- test:coverage
- format
- type-check
- lint
- lint:fix

## 100% test coverage (`npm run test:coverage`)

![image](https://user-images.githubusercontent.com/55896/99860755-ea858700-2b8b-11eb-8b15-2822280f270d.png)

## Preview

### Default state

![image](https://user-images.githubusercontent.com/55896/99860806-156fdb00-2b8c-11eb-869f-977a2dbdc846.png)

### Selected (2 clicked)

![image](https://user-images.githubusercontent.com/55896/99860835-36383080-2b8c-11eb-9f98-ef324bd24dd6.png)

### Tablet view

![image](https://user-images.githubusercontent.com/55896/99860868-4fd97800-2b8c-11eb-8261-e73642759ec4.png)

### Mobile view

![image](https://user-images.githubusercontent.com/55896/99860886-65e73880-2b8c-11eb-9442-e3f1a9e53eab.png)
