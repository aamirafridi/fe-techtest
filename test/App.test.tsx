import React from 'react';
import { fireEvent, render, screen } from '@testing-library/react';

import { App } from '../src/App';

describe('Multiplier App', () => {
  it('should render numbers based on max number provided', () => {
    const { rerender } = render(<App maxNumber={10} />);
    expect(screen.getAllByRole('button', { pressed: false })).toHaveLength(10);
    expect(screen.getByText('1')).toBeInTheDocument();
    expect(screen.queryByText('11')).not.toBeInTheDocument();

    rerender(<App maxNumber={144} />);
    expect(screen.getAllByRole('button', { pressed: false })).toHaveLength(144);
    expect(screen.getByText('1')).toBeInTheDocument();
    expect(screen.queryByText('145')).not.toBeInTheDocument();
  });

  it(`should highlight a number when clicked and also it's multiples`, () => {
    render(<App maxNumber={10} />);
    fireEvent.click(screen.getByText('2'));

    expect(screen.getByText('1')).toHaveAttribute('aria-pressed', 'false');
    expect(screen.getByText('2')).toHaveAttribute('aria-pressed', 'true');
    expect(screen.getByText('3')).toHaveAttribute('aria-pressed', 'false');
    expect(screen.getByText('4')).toHaveAttribute('aria-pressed', 'true');
    expect(screen.getByText('5')).toHaveAttribute('aria-pressed', 'false');
    expect(screen.getByText('6')).toHaveAttribute('aria-pressed', 'true');
    expect(screen.getByText('7')).toHaveAttribute('aria-pressed', 'false');
    expect(screen.getByText('8')).toHaveAttribute('aria-pressed', 'true');
    expect(screen.getByText('9')).toHaveAttribute('aria-pressed', 'false');
    expect(screen.getByText('10')).toHaveAttribute('aria-pressed', 'true');
  });

  it(`should un-highlight an already clicked number when clicked again and also it's multiples`, () => {
    render(<App maxNumber={10} />);

    fireEvent.click(screen.getByText('2'));
    expect(screen.getByText('2')).toHaveAttribute('aria-pressed', 'true');
    expect(screen.getAllByRole('button', { pressed: true })).toHaveLength(5);
    expect(screen.getAllByRole('button', { pressed: false })).toHaveLength(5);

    fireEvent.click(screen.getByText('2'));
    expect(screen.getByText('2')).toHaveAttribute('aria-pressed', 'false');
    expect(screen.queryAllByRole('button', { pressed: true })).toHaveLength(0);
    expect(screen.getAllByRole('button', { pressed: false })).toHaveLength(10);
  });
});
