import './App.css';

import React, { FunctionComponent, useState } from 'react';

type Props = {
  maxNumber: number;
};

export const App: FunctionComponent<Props> = ({ maxNumber }) => {
  const [activeNumber, setActiveNumber] = useState(0);

  return (
    <div className="container">
      {Array.from(Array(maxNumber).keys()).map(num => {
        const number = num + 1;
        return (
          <button
            onClick={() =>
              setActiveNumber(activeNumber === number ? 0 : number)
            }
            key={num}
            aria-pressed={
              activeNumber === number || number % activeNumber === 0
            }
          >
            {number}
          </button>
        );
      })}
    </div>
  );
};
