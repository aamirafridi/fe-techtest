// don't put any source in this file, this is just an entry point for the app.
// you can require things in.
import React from 'react';
import ReactDOM from 'react-dom';

import { App } from './App';

console.log('Welcome to the Which? Javascript exercise!');
console.log(
  'If you are reading this, your Javascript runtime is all up and running correctly.'
);

// I could move this to another file but I assume that this is not considered as source code 😛
ReactDOM.render(
  <React.StrictMode>
    {/* I can move 144 to a json config file but not until it becomes complicated/needed */}
    <App maxNumber={144} />
  </React.StrictMode>,
  document.getElementById('root')
);
